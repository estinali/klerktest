<?php

/** This task take for about one hour - to find decision and decorate it in functions.
 * @author Olga
 * @email olggabalann@gmail.com
 * @var $first string with questions and exclamation marks
 * @var $second string with questions and exclamation marks
 *
 *
 */

/*
Your goal is to make that 6 qu kata from codewars. You should write function balance using all you know about PHP 7 best practices and PSRs. Result should be published on github.

Description: Each exclamation mark weight is 2; Each question mark weight is 3. Put two string left and right to the balance, Are they balanced?
If the left side is more heavy, return "Left"; If the right side is more heavy, return "Right"; If they are balanced, return "Balance".

Examples:
balance("!!","??") === "Right"
balance("!??","?!!") === "Left"
balance("!?!!","?!?") === "Left"
balance("!!???!????","??!!?!!!!!!!") === "Balance"

*/

$first = '?????!!!!';
$second = '!?';

/*First approach
Replace signs, find sum of all digits and compare.
*/
echo compareValuesOption1($first, $second);

/*Second approach
Replace signs, multiple number of replacement to marks values, sum and compare.
*/
echo compareValuesOption2($first, $second);


/*First option functions*/

/**
 * Replaces signs to digits and sums them.
 * @param $string
 * @return int
 */
function stringToSum($string)
{
    $num = str_ireplace(['?', '!'], ['3', '2'], $string);
    return findDigitsSum($num);
}

function findDigitsSum($num)
{
    $sum = 0;
    $rem = 0;
    $strLen = strlen($num);
    $num = (int)$num;

    for ($i = 0; $i <= $strLen; $i++) {
        $rem = $num % 10;
        $sum += $rem;
        $num = $num / 10;
    }
    return $sum;
}

function compareValues($first, $second)
{
    switch ($first <=> $second) {
        case -1:
            return 'Right';
        case 0:
            return 'Balance';
        case 1:
            return 'Left';
    }
}

function compareValuesOption1($string1, $string2)
{
    $firstSum = findDigitsSum($string1);
    $secondSum = findDigitsSum($string2);

    return compareValues($firstSum, $secondSum);
}

/*Second option functions*/

function compareValuesOption2($value1, $value2)
{
    $value1SumByQuestions = getSignValue($value1, '?', 3);
    $value1SumByExcl = getSignValue($value1, '1', 2);

    $value2SumByQuestions = getSignValue($value2, '?', 3);
    $value2SumByExcl = getSignValue($value2, '!', 2);

    return compareValues(($value1SumByQuestions + $value1SumByExcl), ($value2SumByQuestions + $value2SumByExcl));

}

function getSignValue($string, $mark, $markValue)
{
    str_ireplace($mark, $markValue, $string, $replaceNum);
    return $replaceNum * $markValue;

}
