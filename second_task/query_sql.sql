DROP TABLE IF EXISTS `person`;
CREATE TABLE `person` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`surname` varchar(300) DEFAULT NULL,
`name` varchar(300) DEFAULT NULL,
`patronymic` varchar(300) DEFAULT NULL,
`created_at` int(11) DEFAULT NULL,
`updated_at` int(11) DEFAULT NULL,
PRIMARY KEY (`id`),
KEY `idx-surname` (`surname`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

LOCK TABLES `person` WRITE;
INSERT INTO `person` (`surname`, `name`, `patronymic`) VALUES ('Гоголь','Николай','Васильевич'),('Пушкин','Александр','Сергеевич'),('Толстой','Лев','Николаевич');
UNLOCK TABLES;

DROP TABLE IF EXISTS `phone`;
CREATE TABLE `phone` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`number` int(100) NOT NULL,
`person_id` int(11) NOT NULL,
`created_at` int(11) DEFAULT NULL,
`updated_at` int(11) DEFAULT NULL,
PRIMARY KEY (`id`),
KEY `idx-person_id` (`person_id`),
CONSTRAINT `fk-phone-person_id` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


LOCK TABLES `phone` WRITE;
INSERT INTO `phone` (`id`, `number`, `person_id`, `created_at`, `updated_at`) VALUES (2,1090654968,10,1614276462,1614276462);
UNLOCK TABLES;

