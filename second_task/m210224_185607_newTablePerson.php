<?php

use yii\db\Migration;

/**
 * Class m210224_185607_newTablePerson
 */
class m210224_185607_newTablePerson extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('person', [
            'id' => $this->primaryKey(),
            'surname' => $this->string(300),
            'name' => $this->string(300),
            'patronymic' => $this->string(300),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createIndex(
            'idx-surname',
            'person',
            'surname'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('person');
    }

}
