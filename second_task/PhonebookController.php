<?php

namespace backend\controllers;

use backend\models\tables\Person;
use yii\rest\ActiveController;


/**
 * Class PhonebookController provides API functionality for web service
 * @package backend\controllers
 * @link https://www.yiiframework.com/doc/guide/2.0/en/rest-quick-start
 */
class PhonebookController extends ActiveController
{
    public $modelClass = Person::class;

}