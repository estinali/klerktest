<?php

use yii\db\Migration;

/**
 * Class m210224_185638_newTablePhones
 */
class m210224_185638_newTablePhones extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('phone', [
            'id' => $this->primaryKey(),
            'number' => $this->integer(100)->notNull(),
            'person_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createIndex(
            'idx-person_id',
            'phone',
            'person_id'
        );

        $this->addForeignKey(
            'fk-phone-person_id',
            'phone',
            'person_id',
            'person',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-phone-person_id', 'phone');
        $this->dropTable('phone');
    }
}
