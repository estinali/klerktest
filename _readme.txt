Добрый день!
Меня зовут Ольга, представляю выполненное тестовое задание.
Первая задача заняла около часа - на решение и оформление. Спасибо за интересную кату )

Вторая задача - несколько часов.
Sql-код добавления новых таблиц реализован в двух вариантах:
- с помощью инструмента yii2 - миграций 
- и в обычном "сыром" виде sql-запроса.

Код рест-контроллера - с простыми базовыми настройками. Завязан на главную таблицу person.
Данные таблиц person и phone имеют отношение "один ко многим".
Для получения связных данных в файлах моделей настроены соответствующие методы.
Для контроля над тем, какие поля передаются через api, переопреден метод fields() модели Person.
Чтобы в ответе на запрос при необходимости вместе с данными из таблицы person получать связанные телефоны, 
переопределен метод extraFields().



